export const environment = {
    SERVER_URL: `http://localhost:8091`,
    production: true,
    hmr: false,
    useHash: true
};
