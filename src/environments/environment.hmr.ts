export const environment = {
    SERVER_URL: `http://localhost:8091`,
    production: false,
    hmr: true,
    useHash: true
};
