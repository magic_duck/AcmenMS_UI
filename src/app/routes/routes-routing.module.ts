import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { environment } from '@env/environment';
import { LayoutDefaultComponent } from 'app/layout/default/default.component';
import { IndexComponent } from 'app/routes/index/index.component';
import { LayoutPassportComponent } from 'app/layout/passport/passport.component';
import { AuthComponent } from 'app/routes/user/auth/auth.component';
import { Exception403Component } from 'app/routes/exception/403.component';
import { Exception404Component } from 'app/routes/exception/404.component';
import { Exception500Component } from 'app/routes/exception/500.component';

const routes = [
    {
        path: '',
        component: LayoutDefaultComponent,
        children: [
            { path: '', redirectTo: 'index', pathMatch: 'full' },
            { path: 'index', component: IndexComponent },
            { path: 'signin', loadChildren: './signin/signin.module#SigninModule'} // 签到模块
        ]
    },
    // passport
    {
        path: 'user',
        component: LayoutPassportComponent,
        children: [
            { path: 'auth', component: AuthComponent }
        ]
    },
    // 单页不包裹Layout
    { path: '403', component: Exception403Component },
    { path: '500', component: Exception500Component },
    { path: '**', component: Exception404Component },
    // { path: '**', redirectTo: 'index' }
];


@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: environment.useHash })],
    exports: [RouterModule]
  })
export class RouteRoutingModule { }
