import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AjaxResponse } from 'app/model/rest-model';
import { UserAllSignInInfo } from 'app/model/signin-module';

@Component({
  selector: 'app-myself-signin',
  templateUrl: './myself-signin.component.html',
  styles: ['./myselt-signin.component.less']
})
export class MyselfSigninComponent implements OnInit {


  constructor(private http: HttpClient, ) { }

  userAllSignInInfo: UserAllSignInInfo = null;

  options = [];
  selectedOption;

  ngOnInit() {
    this.http.get("/loadUserSignIn").subscribe(
      (val: AjaxResponse<UserAllSignInInfo>) => {
        this.userAllSignInInfo = val.data;
        for (let i = 0; i < 3; i++) {
          let year = parseInt(val.data.curYear) - i;
          this.options.push({ value: `${year}`, label: `${year}年` });
          this.selectedOption = this.options[0];
        }
      }
    );
  }

  selectCurOption(event: any) {
    this.http.get("/loadUserSignIn?year=" + event.value).subscribe(
      (val: AjaxResponse<UserAllSignInInfo>) => {
        this.userAllSignInInfo = val.data;
      }
    );
  }
}
