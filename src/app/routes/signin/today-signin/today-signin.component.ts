import { Component, OnInit, Input, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AjaxResponse, PaginationResult } from 'app/model/rest-model';
import { SignIn } from 'app/model/signin-module';
import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { EventEmitter } from 'selenium-webdriver';

@Component({
  selector: 'app-today-signin',
  templateUrl: './today-signin.component.html',
  styleUrls: ['today-signin.component.less']
})
export class TodaySigninComponent implements OnInit, OnChanges {

  isSpinning = false; // 加载中....
  signInList: SignIn[] = null; // 签到数据
  pageNum = 1; // 当前页
  curPageNum = 0;
  pageTotal; // 总页数
  isSignIn: boolean = true;
  @Input() userSignIn: SignIn; // 用户签到数据

  constructor(
    private http: HttpClient,
  ) { }

  ngOnInit() {
    this.loadCurDaySignIn(0);
  }

  ngOnChanges(): void {
    if (this.userSignIn != null) {
      if (this.signInList == null) {
        this.signInList = [];
      }
      this.signInList.unshift(this.userSignIn);
    }
  }

  loadCurDaySignIn(pageNum: number) {
    if (this.curPageNum != this.pageNum) {
      this.isSpinning = true;
      this.http.get(`/loadCurDaySignIn?pageNum=` + pageNum).subscribe(
        (val: AjaxResponse<PaginationResult<SignIn>>) => {
          if (val.responseCode.code === 200) { // 登录成功
            this.signInList = val.data.list;
            if (this.signInList.length == 0) {
              this.signInList = null;
            }
            this.pageTotal = val.data.page.totalPage * 10;
            this.curPageNum = pageNum;
          }
          this.isSpinning = false;
        }
      );
    }
  }

  updatePageNum() {
    this.loadCurDaySignIn(this.pageNum);
  }
}
