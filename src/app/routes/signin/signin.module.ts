import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { SigninRoutingModule } from 'app/routes/signin/signin-routing.module';
import { SigninComponent } from 'app/routes/signin/signin/signin.component';
import { TodaySigninComponent } from './today-signin/today-signin.component';
import { MyselfSigninComponent } from './myself-signin/myself-signin.component';

@NgModule({
  imports: [
    SharedModule,
    SigninRoutingModule
  ],
  declarations: [
    SigninComponent,
    TodaySigninComponent,
    MyselfSigninComponent,
]
})
export class SigninModule { }