import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {SharedModule} from '@shared/shared.module';
import {environment} from '@env/environment';
import { RouteRoutingModule } from 'app/routes/routes-routing.module';

import {IndexComponent} from './index/index.component';
import {AuthComponent} from './user/auth/auth.component';
import {Exception403Component} from './exception/403.component';
import {Exception404Component} from './exception/404.component';
import {Exception500Component} from './exception/500.component';
import {IndexBbsComponent} from 'app/routes/index/components/index-bbs/index-bbs.component';

@NgModule({
    imports: [SharedModule,RouteRoutingModule],
    declarations: [
        IndexComponent,
        IndexBbsComponent,
        AuthComponent,
        Exception403Component,
        Exception404Component,
        Exception500Component,
    ],
    exports: [
        RouterModule
    ]
})
export class RoutesModule {}
