import { NgModule } from '@angular/core';

import { SharedModule } from '@shared/shared.module';
import { LayoutDefaultComponent } from './default/default.component';
import { HeaderComponent } from './default/header/header.component';
import { FooterComponent } from './default/footer/footer.component';
import { HeaderNotifyComponent } from './default/header/components/notify.component';
import { HeaderIconComponent } from './default/header/components/icon.component';
import { HeaderUserComponent } from './default/header/components/user.component';
import { HeaderLoginComponent } from 'app/layout/default/header/components/login.component';

const COMPONENTS = [
    LayoutDefaultComponent,
    HeaderComponent,
    FooterComponent
];

const HEADERCOMPONENTS = [
    HeaderNotifyComponent,
    HeaderIconComponent,
    HeaderUserComponent,
    HeaderLoginComponent
];

// passport
import { LayoutPassportComponent } from './passport/passport.component';


const PASSPORT = [
    LayoutPassportComponent
];

@NgModule({
    imports: [SharedModule],
    providers: [],
    declarations: [
        ...COMPONENTS,
        ...HEADERCOMPONENTS,
        ...PASSPORT
    ],
    exports: [
        ...COMPONENTS,
        ...PASSPORT
    ]
})
export class LayoutModule { }
