import { Component } from '@angular/core';

@Component({
    selector: 'header-icon',
    template: `
    <nz-dropdown nzTrigger="click" nzPlacement="bottomRight" (nzVisibleChange)="change()">
        <div class="item" nz-dropdown>
            <i class="anticon anticon-appstore-o"></i>
        </div>
        <div nz-menu class="wd-xl animated jello">
            <nz-spin [nzSpinning]="loading" [nzTip]="'正在读取数据...'">
                <div nz-row [nzType]="'flex'" [nzJustify]="'center'" [nzAlign]="'middle'" class="app-icons">
                    <div nz-col [nzSpan]="6" [routerLink]="['/index']">
                        <i class="anticon anticon-calendar bg-error text-white"></i>
                        <small>首页</small>
                    </div>
                    <div nz-col [nzSpan]="6" [routerLink]="['/signin']">
                        <i class="anticon anticon-file bg-teal text-white"></i>
                        <small>签到</small>
                    </div>
                    <div nz-col [nzSpan]="6" [routerLink]="['/spitslot']">
                        <i class="anticon anticon-cloud bg-success text-white"></i>
                        <small>吐槽</small>
                    </div>
                    <div nz-col [nzSpan]="6" [routerLink]="['/exam']">
                        <i class="anticon anticon-star-o bg-pink text-white"></i>
                        <small>在线考试</small>
                    </div>
                    <div nz-col [nzSpan]="6" [routerLink]="['/knowledge']">
                        <i class="anticon anticon-team bg-purple text-white"></i>
                        <small>知识库</small>
                    </div>
                    <div nz-col [nzSpan]="6" [routerLink]="['/bbs']">
                        <i class="anticon anticon-scan bg-warning text-white"></i>
                        <small>讨论区</small>
                    </div>
                    <div nz-col [nzSpan]="6" [routerLink]="['/ask']">
                        <i class="anticon anticon-pay-circle-o bg-cyan text-white"></i>
                        <small>问答</small>
                    </div>
                    <div nz-col [nzSpan]="6" [routerLink]="['/blog']">
                        <i class="anticon anticon-printer bg-grey text-white"></i>
                        <small>博客</small>
                    </div>
                </div>
            </nz-spin>
        </div>
    </nz-dropdown>
    `,
    styles: [`
        
    `]
})
export class HeaderIconComponent {

    loading = true;

    change() {
        setTimeout(() => this.loading = false, 500);
    }

}
