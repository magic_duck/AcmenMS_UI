import { Component, ViewChild, Inject } from '@angular/core';
import { SettingsService } from '@delon/theme';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

    isLogin: boolean = false;
    isUser: boolean = true;

    i = 1;

    constructor(
        public settings: SettingsService,
        @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService) { }

    ngOnInit(): void {
        let token =this.tokenService.get();
        if(token.userId != null) {
            this.isUser = false;
            this.isLogin = true;
        } else {
            this.isUser = true;
            this.isLogin = false;
        }
    }

    isActive(curSelect){
        this.i = curSelect;
    }

}
