import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
// delon
import {NgZorroAntdExtraModule} from 'ng-zorro-antd-extra';
import {AlainThemeModule} from '@delon/theme';
import {AlainACLModule} from '@delon/acl';
import {ABCMODULES, ZORROMODULES} from '../delon.module';
import { NzCalendarModule } from 'ng-zorro-antd';
import { AppCalendarModule } from '@shared/calendar/app-calendar.module';

// 第三方模块
const THIRDMODULES = [
    AppCalendarModule
];

// 自定义组件
const COMPONENTS = [];
// 自定义指令
const DIRECTIVES = [];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        ReactiveFormsModule,
        ...ZORROMODULES,
        NgZorroAntdExtraModule,
        AlainThemeModule.forChild(),
        ...ABCMODULES,
        AlainACLModule,
        ...THIRDMODULES
    ],
    declarations: [
        ...COMPONENTS,
        ...DIRECTIVES
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        ...ZORROMODULES,
        NgZorroAntdExtraModule,
        AlainThemeModule,
        ...ABCMODULES,
        AlainACLModule,
        ...THIRDMODULES,
        ...COMPONENTS,
        ...DIRECTIVES
    ]
})
export class SharedModule { }
