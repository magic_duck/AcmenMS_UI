import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppCalendarComponent } from '@shared/calendar/app-calendar.component';

@NgModule({
  imports     : [ CommonModule ],
  declarations: [ AppCalendarComponent ],
  exports     : [ AppCalendarComponent ]
})
export class AppCalendarModule {
}