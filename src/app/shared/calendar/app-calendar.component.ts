import { Component, ViewEncapsulation, OnInit, Input } from '@angular/core';
import * as moment from 'moment';
import { Moment } from 'moment';
import { Clendar4SignIn, DaySignInfo } from 'app/model/signin-module';

@Component({
    selector: 'app-calendar',
    encapsulation: ViewEncapsulation.None,
    template: `<div>
        <div class="calendar-header">
            <span>{{clendar4SignIn.month}}月</span>
        </div>
        <div class="calendar-full">
            <div class="calendar-body">
                <table class="calendar-table">
                    <thead>
                        <tr>
                            <th class="calendar-column-header" *ngFor="let min of listOfWeekName" >
                                <span class="calendar-column-header-inner">{{min}}</span>
                            </th>
                        <tr>
                    </thead>
                    <tbody>
                        <tr *ngFor="let h of height">
                            <td *ngFor="let w of width">
                                <div class="calendar-date" *ngIf="(h-1)*7+w < fristDay; else elseLast">
                                    <!-- 日历前缀多出来的 -->
                                    <div class="calendar-value invalid">{{beforeMonthDays-fristDay+w+1}}</div>
                                </div>
                                <ng-template #elseLast>
                                     <!-- 日历后缀多出来的 -->
                                    <div class="calendar-date" *ngIf="(h-1)*7+w-fristDay+1 > monthDays; else elseVerify">
                                        <div class="calendar-value invalid">{{(h-1)*7+w-monthDays-fristDay+1}}</div>
                                    </div>
                                    <ng-template #elseVerify>
                                        <div class="calendar-date">
                                            <div class="calendar-value" 
                                                [class.success]="dayInfos[(h-1)*7+w-fristDay].signInType"
                                                [class.error]="!dayInfos[(h-1)*7+w-fristDay].signInType"
                                                [class.now]="dayInfos[(h-1)*7+w-fristDay].curDay">{{(h-1)*7+w-fristDay+1}}</div>
                                        </div>
                                    </ng-template>
                                </ng-template>
                            </td>
                        </tr>
                    <tbody>
                </table>
            </div>
        </div>
    </div>
    `,
    styleUrls: ['./app-calendar.component.less'],
})
export class AppCalendarComponent implements OnInit {
    
    @Input() clendar4SignIn: Clendar4SignIn;
    @Input() curYear: string;

    listOfWeekName: string[] = ['周一', '周二','周三','周四','周五','周六','周日'];
    listOfMonthDays: number[] = [];
    
    width: number[] = [1,2,3,4,5,6,7];
    height: number[] = [1,2,3,4,5,6];
    
    dayInfos: DaySignInfo[];
    fristDay: number;
    monthDays: number;
    beforeMonthDays: number;

    ngOnInit(): void {
        this.fristDay = this.clendar4SignIn.fristDay;
        this.monthDays = this.clendar4SignIn.monthDays;
        this.dayInfos = this.clendar4SignIn.dayInfos;
        this.beforeMonthDays = this.clendar4SignIn.beforeMonthDays;
    }
}
