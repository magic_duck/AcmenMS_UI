import { Router } from '@angular/router';
import { Injectable, Injector, Inject } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { MenuService, SettingsService, TitleService } from '@delon/theme';
import { ACLService } from '@delon/acl';
import { ITokenService, DA_SERVICE_TOKEN } from '@delon/auth';
import { AjaxResponse } from 'app/model/rest-model';

/**
 * 用来获取应用所需要的基础数据
 */
@Injectable()
export class StartupService {

    constructor(
        private menuService: MenuService,
        private settingService: SettingsService,
        private aclService: ACLService,
        private titleService: TitleService,
        @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
        private httpClient: HttpClient,
        private injector: Injector) { }

    private viaHttp(resolve: any, reject: any) {
        this.httpClient.get('/initApp').subscribe(
            (val: SettingsService) => {
                // 应用信息：包括站点名、描述、年份
                this.settingService.setApp(val.app);
                // 用户信息：包括姓名、头像、邮箱地址
                this.settingService.setUser(val.user);
                // ACL：设置权限为全量
                this.aclService.setFull(true);
                console.log(val);
            }
        );
        resolve({});
    }

    load(): Promise<any> {
        // only works with promises
        // https://github.com/angular/angular/issues/15088
        // return new Promise((resolve, reject) => {
        //     this.viaHttp(resolve, reject);
        // });
        return null;
    }
}
