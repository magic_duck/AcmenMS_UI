/**
 * 对应 核心模块 core.module.ts
 * 既然是根模块才需要的核心模块，就不允许在其他地方被导入。所以还需要一个防止不小心的方法。
 */
export function throwIfAlreadyLoaded(parentModule: any, moduleName: string) {
  if (parentModule) {
    throw new Error(`${moduleName} has already been loaded. Import Core modules in the AppModule only.`);
  }
}