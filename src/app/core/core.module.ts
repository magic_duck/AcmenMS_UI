import { NgModule, Optional, SkipSelf } from '@angular/core';
import { throwIfAlreadyLoaded } from './module-import-guard';

/**
 * 核心模块
 * 如果你希望有些东西只是在Angular启动时初始化，然后在任何地方都可以用到，那么把这些东西放在这最适宜的。
 */
@NgModule({
  providers: [
  ]
})
export class CoreModule {
  constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
